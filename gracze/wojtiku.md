![Mordka](https://secure.gravatar.com/avatar/f67f11e978060e9b2b90458f7625d725?d=https%3A%2F%2Fd3oaxc4q5k2d6q.cloudfront.net%2Fm%2F8981a4b643b7%2Fimg%2Fdefault_avatar%2F96%2Fuser_blue.png&s=96)

----

Zdobyte trofea:
=======

![surprised player](http://community.playstarbound.com/data/avatars/m/176/176165.jpg?1386766879 "Nie zachował kamiennej twarzy gdy mu oznajmiono, że umawiamy się za pomocą pull-requestów")

----

Grał w |                  Ile razy
-- | :--------------------------------
[Carcassone](../gry/carcassone.md) | 2
[Dominion](../gry/dominion.md) | 1
[Dixit](../gry/dixit.md) | 1

Log:       |
---------- | :-----------------------------------------------
[11-03-2014](../../../pull-request/3) | [Dominion](../gry/dominion.md)
[18-03-2014](../../../pull-request/4) | [Dixit](../gry/dixit.md)
[25-03-2014](../../../pull-request/5) | [Carcassone](../gry/carcassone.md) (x2)