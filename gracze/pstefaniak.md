![Mordka](https://secure.gravatar.com/avatar/498736dc3763ca17d7d6919b083aa67d?d=https%3A%2F%2Fd3oaxc4q5k2d6q.cloudfront.net%2Fm%2Fe284eeb03549%2Fimg%2Fdefault_avatar%2F96%2Fuser_blue.png&s=96)

----

Zdobyte trofea:
=======


----

Grał w                                     |                  Ile razy
------------------------------------------ | :--------------------------------
[Agricola](../gry/agricola.md)             | 1 
[Dominion](../gry/dominion.md)             | 1 
[Gra o tron](../gry/gra-o-tron.md)         | 1 
[Age of Steam](../gry/age-of-steam.md)     | 1 

Log:       |
---------- | :-----------------------------------------------
[04-03-2014](../../../pull-request/2) | [Gra o tron](../gry/gra-o-tron.md)
[11-03-2014](../../../pull-request/3) | [Dominion](../gry/dominion.md)
[01-04-2014](../../../pull-request/6) | [Agricola](../gry/agricola.md)
[08-04-2014](../../../pull-request/7) | [Age of Steam](../gry/age-of-steam.md)