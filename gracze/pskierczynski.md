![Mordka](https://secure.gravatar.com/avatar/cd733796f1b0da081b7183cb04fdfbda?d=https%3A%2F%2Fd3oaxc4q5k2d6q.cloudfront.net%2Fm%2Fe284eeb03549%2Fimg%2Fdefault_avatar%2F96%2Fuser_blue.png&s=96)

----

Zdobyte trofea:
=======

![Człowiek-wagon](http://icons.iconarchive.com/icons/iconshock/real-vista-transportation/96/tank-wagon-icon.png "Człowiek-wagon [trzeba przyjść po niego i wyciągnąć sprzed komputera bo sam nie wyjdzie]")

----

Grał w |                  Ile razy
-------------------------------------- | :--------------------------------
[Age of Steam](../gry/age-of-steam.md) | 1 ![win](http://www.iconsdb.com/icons/download/black/chess-18-16.png)
[Gra o Tron](../gry/gra-o-tron.md)     | 1 
[Dixit](../gry/dixit.md)               | 1 


Log:       |
---------- | :-----------------------------------------------
[04-03-2014](../../../pull-request/2) | [Gra o tron](../gry/gra-o-tron.md)
[18-03-2014](../../../pull-request/4) | [Dixit](../gry/dixit.md)
[08-04-2014](../../../pull-request/7) | [Age of Steam](../gry/age-of-steam.md)