![Mordka](https://secure.gravatar.com/avatar/042239ca6eadb086ef38ce90be5450b2?d=https%3A%2F%2Fd3oaxc4q5k2d6q.cloudfront.net%2Fm%2Fe284eeb03549%2Fimg%2Fdefault_avatar%2F96%2Fuser_blue.png&s=96)

----

Zdobyte trofea:
=======


----

Grał w                                 |                  Ile razy
-------------------------------------- | :--------------------------------
[Agricola](../gry/agricola.md)         | 1 ![win](http://www.iconsdb.com/icons/download/black/chess-18-16.png "Wygrana")
[Gra o tron](../gry/gra-o-tron.md)     | 1 
[Age of Steam](../gry/age-of-steam.md) | 1

Log:       |
---------- | :-----------------------------------------------
[04-03-2014](../../../pull-request/2) | [Gra o tron](../gry/gra-o-tron.md)
[01-04-2014](../../../pull-request/6) | [Agricola](../gry/agricola.md) ![win](http://www.iconsdb.com/icons/download/black/chess-18-16.png "Wygrana")
[08-04-2014](../../../pull-request/7) | [Age of Steam](../gry/age-of-steam.md)