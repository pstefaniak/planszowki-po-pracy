![Mordka](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2013/Jul/18/pborkowski-avatar-3476648079-3.png)

----

Zdobyte trofea:
=======

----

Grał w |                  Ile razy
-- | :--------------------------------
[Carcassone](../gry/carcassone.md) | 2
[Dominion](../gry/dominion.md) | 2
[Agricola](../gry/agricola.md) | 1

Log:       |
---------- | :-----------------------------------------------
[11-03-2014](../../../pull-request/3) | [Dominion](../gry/dominion.md) (x2)
[25-03-2014](../../../pull-request/5) | [Carcassone](../gry/carcassone.md) (x2)
[01-04-2014](../../../pull-request/6) | [Agricola](../gry/agricola.md)