![Mordka](https://secure.gravatar.com/avatar/68ec8e51e66886bf81483147124a9658?d=https%3A%2F%2Fd3oaxc4q5k2d6q.cloudfront.net%2Fm%2Fe284eeb03549%2Fimg%2Fdefault_avatar%2F96%2Fuser_blue.png&s=96)

----

Zdobyte trofea:
=======


----

Grał w |                  Ile razy
-- | :--------------------------------
[Carcassone](../gry/carcassone.md)     | 2 ![win](http://www.iconsdb.com/icons/download/black/chess-18-16.png "Wygrana") ![win](http://www.iconsdb.com/icons/download/black/chess-18-16.png)
[Dominion](../gry/dominion.md)         | 2 ![win](http://www.iconsdb.com/icons/download/black/chess-18-16.png "Wygrana")
[Gra o tron](../gry/gra-o-tron.md)     | 1 
[Dixit](../gry/dixit.md)               | 1
[Agricola](../gry/agricola.md)         | 1
[Age of Steam](../gry/age-of-steam.md) | 1

Log:       |
---------- | :-----------------------------------------------
[04-03-2014](../../../pull-request/2) | [Gra o tron](../gry/gra-o-tron.md)
[11-03-2014](../../../pull-request/3) | [Dominion](../gry/dominion.md) (x2) ![win](http://www.iconsdb.com/icons/download/black/chess-18-16.png)
[18-03-2014](../../../pull-request/4) | [Dixit](../gry/dixit.md)
[25-03-2014](../../../pull-request/5) | [Carcassone](../gry/carcassone.md) (x2) ![win](http://www.iconsdb.com/icons/download/black/chess-18-16.png) ![win](http://www.iconsdb.com/icons/download/black/chess-18-16.png)
[01-04-2014](../../../pull-request/6) | [Agricola](../gry/agricola.md)
[08-04-2014](../../../pull-request/7) | [Age of Steam](../gry/age-of-steam.md)
