![Mordka](https://secure.gravatar.com/avatar/52c6ead65565b13f3408cc1ea465a13f?d=https%3A%2F%2Fd3oaxc4q5k2d6q.cloudfront.net%2Fm%2Fe284eeb03549%2Fimg%2Fdefault_avatar%2F96%2Fuser_blue.png&s=96)

----

Zdobyte trofea:
=======
![04-03-2014](http://community.playstarbound.com/data/resource_icons/1/1204.jpg?1390510126 "Zwycięzca w grze o tron")

----

Exp: |                  
-- | :--------------------------------
[Carcassone](../gry/carcassone.md)     | 2 
[Dominion](../gry/dominion.md)     | 2 
[Gra o tron](../gry/gra-o-tron.md) | 1 ![win](http://www.iconsdb.com/icons/download/black/chess-18-16.png "Wygrana")
[Dixit](../gry/dixit.md)           | 1


Log:       |
---------- | :-----------------------------------------------
[04-03-2014](../../../pull-request/2) | [Gra o tron](../gry/gra-o-tron.md) ![win](http://www.iconsdb.com/icons/download/black/chess-18-16.png "Wygrana") 
[11-03-2014](../../../pull-request/3) | [Dominion](../gry/dominion.md) (x2)
[18-03-2014](../../../pull-request/4) | [Dixit](../gry/dixit.md)
[25-03-2014](../../../pull-request/5) | [Carcassone](../gry/carcassone.md) (x2)